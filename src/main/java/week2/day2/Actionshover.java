package week2.day2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Actionshover {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		//driver.manage().timeouts().wait();
		driver.get("http://leafground.com/pages/drag.html");
		driver.manage().window().maximize();
		WebElement sourcee = driver.findElement(By.id("draggable"));
		Actions build=new Actions(driver);
		build.dragAndDropBy(sourcee, 100, 100).perform();
		driver.navigate().to("http://leafground.com/pages/selectable.html");
		//WebElement s = driver.findElement(By.xpath("(//li[contains(@class,'ui-widget-content ui-selectee')])[1]"));
		WebElement s = driver.findElement(By.xpath("//li[text()='Item 1']"));
		WebElement ss = driver.findElement(By.xpath("//li[text()='Item 2']"));
		WebElement sss = driver.findElement(By.xpath("//li[text()='Item 1']"));
		
		
		build.clickAndHold(s).clickAndHold(ss).clickAndHold(sss).perform();
		
	}

}
