package week2.day3;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class Popup {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.manage().window().maximize();	
		driver.switchTo().frame("iframeResult");
        driver.findElementByXPath("//button[Text() = 'Try it']").click();
        Alert alert=driver.switchTo().alert();
        alert.sendKeys("pragadeesh");
        alert.accept();
        driver.findElementById("demo").getText();
        
	}

}
