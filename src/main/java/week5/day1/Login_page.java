package week5.day1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.Annotations;

public class Login_page extends Annotations {
	private static final WebDriver Driver = null;

	public Login_page() {
		PageFactory.initElements(Driver ,  this);
	}

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		WebElement eleUserName = driver.findElementById("username");
		eleUserName.sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElement(By.xpath("(//div[@id='button']//img)[1]")).click();
	}
	
	@FindBy(how = How.ID , using = "companyname")
	
	private WebElement cname;
	
	
	@FindBy(how = How.ID, using="firstname")
	
	private WebElement fname;

	@FindBy(how = How.ID , using = "lastname")
	
	private WebElement lname;
	
	public Login_page enterCompanyName(String ccname)
	{
		ClearAndType(cname , ccname);	}
	
	
}

