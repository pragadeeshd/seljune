package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
		
		features = "src\\test\\java\\feature\\login.feature",
		glue = {"steps","hooks"},
		dryRun = !true,
		
		
		snippets = SnippetType.CAMELCASE,
		monochrome = true,
		plugin= {"pretty","html:cucumberReports"}
		
		)

public class Run extends AbstractTestNGCucumberTests {

}
