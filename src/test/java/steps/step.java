package steps;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class step {
	public static ChromeDriver driver;
	@Given("Start application")
	public void start_application() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver =new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		
	}

	@Given("enter the username as demosalesmanager and password as crmsfa")
	public void enter_the_username_as_demosalesmanager_and_password_as_crmsfa() {
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		
	}

	@When("i click on log in button")
	public void i_click_on_log_in_button() {
		driver.findElementByClassName("decorativeSubmit").click();
	 
	 }

	@Then("verify")
	public void verify() {
		System.out.println("logged in ");
	}
	
	@Given("login")
	public void enter() {
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		
	}
	
	
	@Given("click on the leads and create leads")
	public void clickOnTheLeadsAndCreateLeads() {
		driver.findElementById("label").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
	}


	@Given("enter the companyname as test")
	public void enter_the_companyname_as_test() {
		driver.findElementById("createLeadForm_companyName").sendKeys("test");
	}

	@Given("enter the firstname as pragadeesh")
	public void enter_the_firstname_as_pragadeesh() {
		driver.findElementById("createLeadForm_firstName").sendKeys("pragadeesh");
		
	}

	@Given("enter the surname as D")
	public void enter_the_surname_as_D() {
		driver.findElementById("createLeadForm_lastName").sendKeys("D");
		driver.findElementByClassName("smallSubmit").click();
	}

	@When("i click submit")
	public void i_click_submit() {
		driver.findElementByClassName("smallSubmit").click();
		System.out.println("created");
	}
}
